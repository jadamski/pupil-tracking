import sys, random
from PyQt5 import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PIL import Image
import glob
#import cv2
from main import *
from PyQt5 import QtWidgets
from PyQt5 import *
from uploadVideo import *
import pyqtgraph as pg
from PIL import Image
from numpy import *
import numpy
import math
import csv 
count = 0
image_list = []
frames= 0

class MyMainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
   
    def __init__(self):
        super(self.__class__, self).__init__()
        self.setupUi(self) 
        
        #Connecting buttons
        self.L_button.clicked.connect(self.on_clickLeft)
        self.R_button.clicked.connect(self.on_clickRight)
        self.actionUpload_new.triggered.connect(self.openVidFile)
        self.horizontalSlider.sliderMoved.connect(self.sliderMoved)
        #pg.setConfigOption('background', 'w') #before loading widget
        #self.graphicsView.mousePressEvent = self.getPos #PROBLEM: makes it so that you cannot maneuver around w mouse
     

        #set up display with ViewBox  
        vb = self.graphicsView #this is NOT graphicsView, but viewbox - need to change name in main.py
        vb.enableAutoRange('xy') #the axis will NOT automatically rescale when items are added/removed or change their shape. 
        #gv.autoRange() #DONT NEED -- #Sets the range of the view box to make all children visible
        
        #Opening image into ViewBox
        img = Image.open("Untitled.png")
        arr = array(img)
        arr = np.rot90(arr, -1)
        img_arr = pg.ImageItem(arr)
        vb.addItem(img_arr)
        vb.disableAutoRange('xy')
        vb.autoRange()    
        
        #Draw elipse
        cir = pg.EllipseROI([150, 80], [858, 872], pen=(4,9))
        vb.addItem(cir)
        
        #Prints data about position of elipse, then declaring variables
        print("Position:", cir.pos()) #returns the coordinates of the lower left corner of the bounding box
        print("Array region", cir.getArrayRegion) #used to return the exact image coordinates that are accessed 
        print("Save state:", cir.saveState()) # returns a serializable object that you can use to save/restore the ROI
        print("Array slice:", cir.getArraySlice) #used to determine the region of an image that intersects the ROI
        position = cir.pos()
        arrayRegion = cir.getArrayRegion
        saveState = cir.saveState
        arraySlice = cir.getArraySlice        
        
        #Writing to csv file
        with open ('data.csv', 'w') as csvfile:
            file = csv.writer(csvfile, delimiter=' ')
            file.writerow([position, arrayRegion, saveState, arraySlice])
            
        #Setting save state in next frame
        cir.setState(cir.saveState())
            

        #radius = math.sqrt(((p2-p1)**2)+ (p2-p1**2))
        
        
        
    @pyqtSlot()
    def on_clickLeft(self):
        global count
        if count > 0:
                count= count - 1
                img = pg.ImageItem(asarray(Image.open(image_list[count])) )
                self.graphicsView.addItem(img)
                img.rotate(270)
                self.horizontalSlider.setSliderPosition(count)
                
                
    def on_clickRight(self):
        global count
        global image_list
        if count < len(image_list)-1:
                count = count + 1
                pixmap = QPixmap(image_list[count])
                self.label.setPixmap(pixmap)
                self.horizontalSlider.setSliderPosition(count)
                
                
    def sliderMoved(self, val):
        try:
            count = val

            img = pg.ImageItem(asarray(Image.open(image_list[count])) )
            self.graphicsView.addItem(img)
            img.rotate(270)
            img.setZValue(0)
            
            
        except IndexError:
            print ("Error: No image at index"), val
        
    def keyPressEvent(self, event):
        global count
        global image_list
        key = event.key()
        if key == Qt.Key_A:
            if count > 0:
                count= count - 1 
                img = pg.ImageItem(asarray(Image.open(image_list[count])) )
                self.graphicsView.addItem(img)
                img.rotate(270)
                
                self.horizontalSlider.setSliderPosition(count)
                print ("viewing frame " + str(count))

        elif key == Qt.Key_D:
            if count < len(image_list)-1:
                count = count + 1
                img = pg.ImageItem(asarray(Image.open(image_list[count])) )
                self.graphicsView.addItem(img)
                img.rotate(270)
                
                self.horizontalSlider.setSliderPosition(count)
                print ("viewing frame " + str(count))
            
                

        elif key == Qt.Key_Escape:
            self.close()
            
        #event.accept()  ?
            
        
###################################################################################################
          
    def getPos(self, event):
        cir = pg.EllipseROI([200, 200], [100, 100], pen=(4,9))
        x = event.pos().x()
        y = event.pos().y()
        print ("(",x,",",y,")")
        cir = pg.EllipseROI([x, y], [100, 100], pen=(4,9))
        vb.append(cir)
        #return(x,y)
        #print("Currently viewing image:", image_list[count])
        
   
   #There is probably a simpler way of using tuples in a function    
    def return_x(t):
        x, y = t
        return x
    def return_y(t):
        x, y = t
        return y
    
    
######################################################################################################

    
######################################################################################################
        
    def openVidFile(self):
        fileName = openFile() #openFile() opens file browser and returns name of selected video file
        directory = str(QFileDialog.getExistingDirectory(self, "Select Folder to Store Frames"))
         
        splitVideo(fileName, image_list, directory)
        for filename in glob.glob(directory + '\\*.jpg'):
            image_list.append(filename)
           
        self.horizontalSlider.setRange(0,len(image_list)-1)
        

    

def main():
    app = QtWidgets.QApplication(sys.argv)  # A new instance of QApplication
    form = MyMainWindow()  # We set the form to be our ExampleApp (design)
    form.show()  # Show the form
    app.exec_()  # and execute the app


if __name__ == '__main__':  # if we're running file directly and not importing it
    main()  # run the main function